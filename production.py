import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pylab import rcParams
import warnings
import mlflow
from math import sqrt
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
warnings.filterwarnings('ignore')
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix
import mlflow.pyfunc
import dvc.api

mlflow.set_tracking_uri("http://65.0.194.230:5000")

model_name = 'SS_Pro_RF1'
model_version_uri = "models:/{model_name}/production".format(model_name=model_name)
loaded_model = mlflow.pyfunc.load_model(model_version_uri)

# path2 = 'Data//next_month.csv'
# repo2 = 'C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection' 
# version2 = 'v2'

# data_url2 = dvc.api.get_url(
#     path=path2,
#     repo= repo2,
#     rev= version2  
# )
path = 'Data//Phase4_old.csv'
repo = 'https://gitlab.com/Devendra_pal/sasol_pro' 
version = 'v1'

data_url = dvc.api.get_url(
    path=path,
    repo= repo,
    # rev= version  
)
def calculate_psi(expected, actual, buckettype='bins', buckets=10, axis=0):
    def psi(expected_array, actual_array, buckets):
        def scale_range (input, min, max):
            input += -(np.min(input))
            input /= np.max(input) / (max - min)
            input += min
            return input
        breakpoints = np.arange(0, buckets + 1) / (buckets) * 100
        if buckettype == 'bins':
            breakpoints = scale_range(breakpoints, np.min(expected_array), np.max(expected_array))
        elif buckettype == 'quantiles':
            breakpoints = np.stack([np.percentile(expected_array, b) for b in breakpoints])
        expected_percents = np.histogram(expected_array, breakpoints)[0] / len(expected_array)
        actual_percents = np.histogram(actual_array, breakpoints)[0] / len(actual_array)
        def sub_psi(e_perc, a_perc):
            if a_perc == 0:
                a_perc = 0.0001
            if e_perc == 0:
                e_perc = 0.0001
            value = (e_perc - a_perc) * np.log(e_perc / a_perc)
            return(value)
        psi_value = np.sum(sub_psi(expected_percents[i], actual_percents[i]) for i in range(0, len(expected_percents)))

        return(psi_value)

    if len(expected.shape) == 1:
        psi_values = np.empty(len(expected.shape))
    else:
        psi_values = np.empty(expected.shape[axis])

    for i in range(0, len(psi_values)):
        if len(psi_values) == 1:
            psi_values = psi(expected, actual, buckets)
        elif axis == 0:
            psi_values[i] = psi(expected[:,i], actual[:,i], buckets)
        elif axis == 1:
            psi_values[i] = psi(expected[i,:], actual[i,:], buckets)

    return(psi_values)
# mlflow.set_experiment('Credit_card-fraud')

if __name__ == "__main__":
    data=pd.read_csv(data_url, sep=",")
    df1 = data[:-5]
    df = df1.drop(['Unnamed: 0', 'timestamp'],axis=1)
    raw,col = df.shape
    # print(raw)
    df.set_index('DateTime', inplace = True)  
    # df["NaSCN_1D"] = df.NaSCN.shift(-1)
    # df_porod1 = pd.read_csv('C://Users//DevendraP//Desktop//mlops-sasol//sasol_pro//Data//Phase4_new.csv')
    df_porod1 = pd.read_csv('Data/Phase4_new.csv')
    df_porod1 = df_porod1.drop(['Unnamed: 0', 'timestamp'],axis=1)
    df_porod1.set_index('DateTime', inplace = True)  
    t =df_porod1.shape
    Y_NaSCN_1D= df_porod1['NaSCN'] 
    sc = RobustScaler()
    training_set_scaled = sc.fit_transform(df_porod1)
    thirty_day = pd.Series(Y_NaSCN_1D[0:30]) 
    client = mlflow.tracking.MlflowClient(tracking_uri = 'http://65.0.194.230:5000')
    mlflow.set_tracking_uri('http://65.0.194.230:5000')
    mlflow.set_experiment('sasol_pro')
    with mlflow.start_run(run_name='prod') as mlops_run:
        predictions_xgb = loaded_model.predict(training_set_scaled)
        rmse_rf = sqrt(mean_squared_error(thirty_day,predictions_xgb))
        mae_rf = mean_absolute_error(thirty_day,predictions_xgb)
        r2_score1 = r2_score(thirty_day,predictions_xgb)
        print(rmse_rf , mae_rf, r2_score1 )
        with open("metrics.txt", "w") as outfile:
            outfile.write("rmse_rf: " + str(rmse_rf) + "\n")
            outfile.write("mae_rf: " + str(mae_rf) + "\n")
            outfile.write("r2_score1: " + str(r2_score1) + "\n")
        mlflow.log_param('Model_Name', 'Linear_reg')
        mlflow.log_param('Data_version','v2')
        mlflow.log_param('input_row', t[0])
        mlflow.log_param('input_cols', t[1])
        mlflow.log_metric("RMSE",rmse_rf)
        mlflow.log_metric("MAE",mae_rf)
        mlflow.log_metric("R2",r2_score1)
        plt.figure(figsize=(20,12)) 
        plt.xticks(rotation=90)
        plt.plot(df.index[0:30],thirty_day,color ='tab:blue')
        plt.plot(df.index[0:30],thirty_day,color ='tab:orange')
        plt.legend(["predicted", "original"],prop={'size': 40})
        plt.savefig("results1.png")
        mlflow.log_artifact("results1.png")
        psi_list = []
        col = df_porod1.columns
        for feature in col:
                # Assuming you have a validation and training set
                psi_t = calculate_psi(df[feature], df_porod1[feature])
                psi_list.append(psi_t)      
                print(feature,psi_t)
                mlflow.log_param(feature,psi_t)
                if psi_t>0.05:
                    print("Need to retrain the model")
                    with open("PSI.txt", "w+") as outfile:
                        outfile.write("feature: " + str(psi_t) + "\n")
   
    
    


    