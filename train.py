from asyncio.log import logger
# from ensurepip import version
from importlib.resources import path
import pandas as pd
import numpy as np 
import xgboost
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import ElasticNet
from urllib.parse import urlparse
import mlflow
import mlflow.sklearn
import logging 
from math import sqrt
from sklearn.preprocessing import RobustScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
import matplotlib.pyplot as plt
import pickle
logging.basicConfig(level=logging.WARN)
logger =  logging.getLogger(__name__)

import dvc.api

path = 'Data//Phase4_old.csv'
repo = 'https://gitlab.com/Devendra_pal/sasol_pro' 
# version = 'v1'

data_url = dvc.api.get_url(
    path=path,
    repo= repo,
    # rev= version  
)
mlflow.set_experiment('sasol_pro')

def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual,pred))
    mae= mean_absolute_error(actual,pred)
    r2 = r2_score(actual,pred)
    return rmse, mae, r2

if __name__ == "__main__":

    
    # data=pd.read_csv('C://Users//DevendraP//Desktop//credit_card//Credit-Card-Fraud-Detection//Data//credit_card.csv')
    data=pd.read_csv(data_url, sep=",")
    print(data.columns)
    df1 = data[:-5]
    df = df1.drop(['Unnamed: 0', 'timestamp'],axis=1)
    raw,col = df.shape
    print(df.columns)
    df.set_index('DateTime', inplace = True)  
    df["NaSCN_1D"] = df.NaSCN.shift(-1) 
    training_set_1d = df.drop(['NaSCN_1D'],axis=1)
    t = training_set_1d.shape
    # print(t)
    Y_NaSCN_1D= df['NaSCN_1D'] 
    sc = RobustScaler()
    training_set_scaled = sc.fit_transform(training_set_1d)
    client = mlflow.tracking.MlflowClient(tracking_uri = 'http://65.0.194.230:5000')
    mlflow.set_tracking_uri('http://65.0.194.230:5000')
    mlflow.set_experiment('sasol_pro')
    with mlflow.start_run(run_name='test1') as mlops_run:
        predicted_y = []
        for i in range(30):
            X_train_scaled = training_set_scaled[:470+i]
            # print(np.isnan(X_train_scaled))
            # if np.isnan(X_train_scaled).any()== True:
            #     print(np.isnan(X_train_scaled))
            #     print(i)
            X_test_scaled  = training_set_scaled[470+i:471+i]
            y_train = Y_NaSCN_1D[:470+i]
            y_test = Y_NaSCN_1D[470 +i]  
            # print(y_train)
            reg = xgboost.XGBRegressor(objective='reg:squarederror', \
                                    n_estimators=1000, \
                                    nthread=24)
#             reg = RandomForestRegressor(n_estimators = 100) 
            reg = LinearRegression()
            reg.fit(X_train_scaled, y_train)
            predictions_xgb = reg.predict(X_test_scaled)
            predicted_y.append(predictions_xgb) 
        # filename = 'model_Linear_v1.pkl'
        # pickle.dump(reg, open(filename, 'wb'))
        ten_day_og = Y_NaSCN_1D[470:500]
        ten_day_pred = pd.Series(predicted_y) 
        # rmse_xgb = sqrt(mean_squared_error(ten_day_og,ten_day_pred))
        # mae_xgb = mean_absolute_error(ten_day_og,ten_day_pred)
        rmse_rf = sqrt(mean_squared_error(ten_day_og,ten_day_pred))
        mae_rf = mean_absolute_error(ten_day_og,ten_day_pred)
        r2_score1 = r2_score(ten_day_og,ten_day_pred)
        print(rmse_rf , mae_rf, r2_score1 )
        with open("metrics.txt", "w") as outfile:
            outfile.write("rmse_rf: " + str(rmse_rf) + "\n")
            outfile.write("mae_rf: " + str(mae_rf) + "\n")
            outfile.write("r2_score1: " + str(r2_score1) + "\n")
            # outfile.write("f1_score: " + str(f1_score) + "\n")
#         mlflow.log_param("data_url", data_url)
        mlflow.log_param('Model_Name', 'XgBoost')
        mlflow.log_param('Data_version','v1')
        mlflow.log_param('input_row', t[0])
        mlflow.log_param('input_cols', t[1])
        mlflow.log_metric("RMSE",rmse_rf)
        mlflow.log_metric("MAE",mae_rf)
        mlflow.log_metric("R2",r2_score1)
        # mlflow.log_artifact('model_xgb_v1.pkl')
        plt.figure(figsize=(20,12)) 
        plt.xticks(rotation=90)
        plt.plot(df.index[470:500],ten_day_pred,color ='tab:blue')
        plt.plot(df.index[470:500],ten_day_og,color ='tab:orange')
        plt.legend(["predicted", "original"],prop={'size': 30})
        plt.savefig("results1.png")
        mlflow.log_artifact("results1.png")

        tracking_url_type_store = urlparse(mlflow.get_artifact_uri()).scheme
        print(tracking_url_type_store)
        if tracking_url_type_store != 'file':
            mlflow.sklearn.log_model(reg ,'model', registered_model_name = 'SS_Pro_XGBoost')
        else:
            pass


